package com.example.studyhelper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studyhelper.Models.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 정영인 on 10/10/2017.
 */

public class QuestionListArrayAdapter extends ArrayAdapter {
    private ArrayList<Question> questions;
    private Context context;
    private int subjectID;
    private int currentUserId;


    public QuestionListArrayAdapter(@NonNull Context context, @LayoutRes int resource, int subjectID, @NonNull ArrayList<Question> questions, int currentUserId)  {
        super(context, resource, questions);
        this.questions =questions;
        this.context = context;
        this.subjectID = subjectID;
        this.currentUserId = currentUserId;
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Nullable
    @Override
    public Question getItem(int position) {
        return questions.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.question_list_item,parent,false);
        }

        TextView questionTextView = (TextView) convertView.findViewById(R.id.question_text_view);
        questionTextView.setText(getItem(position).getQuestion());
        TextView nameTextView = (TextView) convertView.findViewById(R.id.question_user_name);
        nameTextView.setText("- " + getItem(position).getUser().getFirstName() + " " +getItem(position).getUser().getLastName());
        nameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent googleMapIntent = new Intent(context, MapsActivity.class);
                googleMapIntent.putExtra("currentUserId", getItem(position).getUser().getUserId());
                context.startActivity(googleMapIntent);
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int questionID = position;

                Intent answerActivityIntent = new Intent(context, AnswerActivity.class);
                answerActivityIntent.putExtra("subjectID", subjectID);
                answerActivityIntent.putExtra("questionID",questionID);
                answerActivityIntent.putExtra("currentUserId", currentUserId);
                context.startActivity(answerActivityIntent);
            }
        });
        return convertView;
    }
}

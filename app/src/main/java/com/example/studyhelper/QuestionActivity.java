package com.example.studyhelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studyhelper.Models.DatabaseSimulator;
import com.example.studyhelper.Models.Question;
import com.example.studyhelper.Models.Subject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class QuestionActivity extends AppCompatActivity
{
    DatabaseSimulator databaseSimulator;
    private int subjectId, userId;

    private static int REQUEST_QA_CREATION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        databaseSimulator = new DatabaseSimulator();


        Bundle bundle = getIntent().getExtras();
        subjectId = bundle.getInt("subjectId");
        userId = bundle.getInt("userId");

        TextView subjectTitleView = (TextView) findViewById(R.id.subject_title) ;
        subjectTitleView.setText(databaseSimulator.getSubjects().get(subjectId).getSubject());

        initializeQuestionList();
    }

    public void initializeQuestionList()
    {
        ArrayList<Subject> subjects = new ArrayList<>();
        subjects = databaseSimulator.getSubjects();
        ArrayList<String> question_strings = new ArrayList<>();

        Subject currentSubject = subjects.get(subjectId);

        ListView listView = (ListView) findViewById(R.id.question_list_view);
        listView.setAdapter(new QuestionListArrayAdapter(this,R.layout.question_list_item,subjectId, currentSubject.getQuestions(), userId));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent createQuestionIntent = new Intent(QuestionActivity.this, QACreationActivity.class);
                createQuestionIntent.putExtra("contentType", 0);
                createQuestionIntent.putExtra("subjectId", subjectId);
                startActivityForResult(createQuestionIntent, REQUEST_QA_CREATION);

                Snackbar.make(view, "Your questions has been posted.", Snackbar.LENGTH_LONG)

                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_QA_CREATION) {
            Bundle bundle = data.getExtras();
            String content = bundle.getString("content");
            Question question = new Question(databaseSimulator.getUsers().get(userId), content);
            databaseSimulator.addQuestionToSubject(subjectId, question);
            initializeQuestionList();
        }
    }
}

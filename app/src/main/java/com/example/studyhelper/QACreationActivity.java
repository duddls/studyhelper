package com.example.studyhelper;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class QACreationActivity extends AppCompatActivity
{
    private int contentType, subjectId, questionId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qacreation);

        Bundle bundle = getIntent().getExtras();
        // ContentType - 0 is question creation, 1 is answer.
        contentType = bundle.getInt("content_type");
        subjectId = bundle.getInt("subjectId");

        TextView contentTitle = (TextView) findViewById(R.id.content_creation_title);
        contentTitle.setText("Ask a Question");
        // Question Creation
        if (contentType == 1) {
            questionId = bundle.getInt("questionId");
            contentTitle.setText("Answer the Question");
        }
    }

    public void createContent(View v)
    {
        EditText editText = (EditText) findViewById(R.id.intent_data);

        if (editText.getText().toString().length() < 10) {
            Toast.makeText(this, "Your content must be of a decent length.", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent();
        intent.putExtra("content", editText.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }
}

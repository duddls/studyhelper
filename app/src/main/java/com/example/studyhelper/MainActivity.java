package com.example.studyhelper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.example.studyhelper.Models.DatabaseSimulator;
import com.example.studyhelper.Models.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    private ArrayList<User> users;
    private DatabaseSimulator databaseSimulator;
    private CallbackManager callbackManager;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseSimulator = new DatabaseSimulator();
        users = databaseSimulator.getUsers();

        callbackManager = CallbackManager.Factory.create();

        sharedPreferences  = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);

        String username = sharedPreferences.getString("username", "");
        String password = sharedPreferences.getString("password", "");
        if(username.length() > 0 && password.length() > 0) {
            int userId = isIDorPasswordCorrect(username, password);
            if (userId != -1) {
                Intent subjectIntent = new Intent(this, SubjectActivity.class);
                subjectIntent.putExtra("userId", userId);
                startActivity(subjectIntent);
            } else {
                Toast.makeText(this, "Your ID is not correct! Try again.", Toast.LENGTH_SHORT).show();
            }
        }

        AccessToken token = AccessToken.getCurrentAccessToken();

        if (token != null) {
            Intent subjectIntent = new Intent(this, SubjectActivity.class);
            subjectIntent.putExtra("userId", 0);
            startActivity(subjectIntent);
        }

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Intent subjectIntent = new Intent(MainActivity.this, SubjectActivity.class);
                subjectIntent.putExtra("userId", 0);
                startActivity(subjectIntent);

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void doLogin(View view) {
        AutoCompleteTextView usernameView = (AutoCompleteTextView) findViewById(R.id.login_username);
        AutoCompleteTextView passwordView = (AutoCompleteTextView) findViewById(R.id.login_password);
        String username = usernameView.getText().toString();
        String password = passwordView.getText().toString();

        int userId = isIDorPasswordCorrect(username, password);
        if (userId != -1) {
            Intent subjectIntent = new Intent(this, SubjectActivity.class);
            subjectIntent.putExtra("userId", userId);
            startActivity(subjectIntent);
        } else {
            Toast.makeText(this, "Your ID is not correct! Try again.", Toast.LENGTH_SHORT).show();
        }
    }

    public int isIDorPasswordCorrect(String username, String password) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getUsername().equals(username) && users.get(i).getPassword().equals(password)){
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("username", username);
                editor.putString("password", password);
                editor.commit();
                return i;
            }
        }

        return -1;
    }

}

package com.example.studyhelper;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.studyhelper.Models.Subject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 정영인 on 10/4/2017.
 */

public class SubjectListArrayAdapter extends ArrayAdapter
{
    private Context mContext;
    private ArrayList<Subject> subjects;
    private int userId;

    public SubjectListArrayAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Subject> subjects, int userId)
    {
        super(context, resource, subjects);

        this.mContext = context;
        this.subjects = subjects;
        this.userId = userId;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.subject_list_item, parent, false);
        }

        TextView subjectNameView = (TextView) convertView.findViewById(R.id.subject_list_item_name);
        subjectNameView.setText(subjects.get(position).getSubject());

        convertView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent questionsIntent = new Intent(mContext, QuestionActivity.class);
                questionsIntent.putExtra("subjectId", position);
                questionsIntent.putExtra("userId", userId);
                mContext.startActivity(questionsIntent);
            }
        });

        return convertView;
    }
}

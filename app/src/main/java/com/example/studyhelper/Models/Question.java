package com.example.studyhelper.Models;

import java.util.ArrayList;

/**
 * Created by 정영인 on 10/4/2017.
 */

public class Question {
    private ArrayList<Answer> answers;
    private String question;
    private User user;

    public Question(User user, String q)
    {
        question = q;
        answers = new ArrayList<>();
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public String getQuestion() {
        return question;
    }

    public void addAnswer(Answer a)
    {
        answers.add(a);
    }
}

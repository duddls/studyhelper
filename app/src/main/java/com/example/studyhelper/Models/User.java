package com.example.studyhelper.Models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by 정영인 on 10/9/2017.
 */

public class User
{
    private String username, password;
    private String FirstName, LastName;
    private LatLng latLng;
    private int userId;


    public User(int userId, String username, String password, String FirstName, String LastName, LatLng latLng)
    {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.latLng = latLng;
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}

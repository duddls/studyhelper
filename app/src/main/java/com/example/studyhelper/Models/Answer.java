package com.example.studyhelper.Models;

/**
 * Created by 정영인 on 10/4/2017.
 */

public class Answer {
    private User user;
    private String answer;

    public Answer(User user, String answer)
    {
        this.user = user;
        this.answer = answer;
    }

    public User getUser() {
        return user;
    }

    public String getAnswer() {
        return answer;
    }
}

package com.example.studyhelper.Models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by 정영인 on 10/9/2017.
 */

public class DatabaseSimulator
{
    ArrayList<User> users = new ArrayList<>();
    ArrayList<Subject> subjects = new ArrayList<>();

    public ArrayList<User> getUsers() {
        return users;
    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }

    public DatabaseSimulator()
    {
        // Create an emulation of users.
        users.add(new User(0, "Yeongin", "1234", "Yeongin", "Jung", new LatLng(1.3521, 103.8198))); // 0
        users.add(new User(1, "Metta", "1234", "Metta", "Adithia", new LatLng(37.5665, 126.9780))); // 1
        users.add(new User(2, "Jason", "1234", "Jason", "Lin C", new LatLng(37.8136, 144.9631)));   // 2
        users.add(new User(3, "Rachel", "1234", "Rachel", "Kim", new LatLng(37.8136, 144.9631)));   //3

        // Create an emulation of english subject, questions and answers.
        Subject english = new Subject("English");
        Question english_q1 = new Question(users.get(1), "What is noun of improve?");
        Answer english_q1_a1 = new Answer(users.get(0), "The noun of improve is improval.");
        Answer english_q1_a2 = new Answer(users.get(2), "No! The noun of improve is improvement.");
        english_q1.addAnswer(english_q1_a1);
        english_q1.addAnswer(english_q1_a2);

        Question english_q2 = new Question(users.get(2), "What is english really?");
        Answer english_q2_a1 = new Answer(users.get(3), "English really, is a subject.");
        Answer english_q2_a2 = new Answer(users.get(1), "English really, is not a subject, but a language.");
        english_q2.addAnswer(english_q2_a1);
        english_q2.addAnswer(english_q2_a2);

        Question english_q3 = new Question(users.get(3),"What is the difference between color and colour?");
        Answer english_q3_a1 = new Answer(users.get(2),"That is because of nations - USA and British.");
        english_q3.addAnswer((english_q3_a1));

        // Add english questions and answers to the subject.
        english.addQuestion(english_q1);
        english.addQuestion(english_q2);
        english.addQuestion((english_q3));

        // Create an emulation of math subject, questions and answers.
        Subject math = new Subject("Math");
        Question math_q1 = new Question(users.get(1), "What is a function in math?");
        Answer math_q1_a1 = new Answer(users.get(0), "Math is a kind of subject.");
        Answer math_q1_a2 = new Answer(users.get(2), ".");
        math_q1.addAnswer(math_q1_a1);
        math_q1.addAnswer(math_q1_a2);

        Question math_q2 = new Question(users.get(0), "What is 1 + 1?");
        Answer math_q2_a1 = new Answer(users.get(1), "The answer is 1.");
        Answer math_q2_a2 = new Answer(users.get(2), "Really? The exact answer is 2.");
        math_q2.addAnswer(math_q2_a1);
        math_q2.addAnswer(math_q2_a2);

        // Add math questions and answers to the subject.
        math.addQuestion(math_q1);
        math.addQuestion(math_q2);

        subjects.add(english);
        subjects.add(math);
    }

    public void addQuestionToSubject(int subjectId, Question question)
    {
        subjects.get(subjectId).addQuestion(question);
    }

    public void addAnswerToQuestionInSubject(int subjectId, int questionId, Answer answer)
    {

    }
}

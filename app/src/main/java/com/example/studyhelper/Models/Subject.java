package com.example.studyhelper.Models;

import java.util.ArrayList;

/**
 * Created by 정영인 on 10/4/2017.
 */

public class Subject {
    private String subject;
    private ArrayList <Question> questions;

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public Subject(String s){
        subject = s;
        questions = new ArrayList<>();
    }
    public void addQuestion(Question q)
    {
        questions.add(q);
    }

    public String getSubject()
    {
        return this.subject;
    }
}

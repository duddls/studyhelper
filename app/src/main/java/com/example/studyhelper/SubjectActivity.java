package com.example.studyhelper;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.studyhelper.Models.Answer;
import com.example.studyhelper.Models.DatabaseSimulator;
import com.example.studyhelper.Models.Question;
import com.example.studyhelper.Models.Subject;

import java.util.ArrayList;

public class SubjectActivity extends AppCompatActivity {
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);

        DatabaseSimulator databaseSimulator = new DatabaseSimulator();

        Bundle bundle = getIntent().getExtras();
        userId = bundle.getInt("userId");
        ListView subjectListView = (ListView) findViewById(R.id.subject_list_view);
        ArrayAdapter<String> arrayAdapter = new SubjectListArrayAdapter(this, R.layout.subject_list_item, databaseSimulator.getSubjects(), userId);
        subjectListView.setAdapter(arrayAdapter);

        Button logoutBtn = (Button) findViewById(R.id.btn_logout);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("username");
                editor.remove("password");
                editor.commit();
                finish();
            }
        });
    }
}

package com.example.studyhelper;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.studyhelper.Models.Answer;
import com.example.studyhelper.Models.DatabaseSimulator;
import com.example.studyhelper.Models.Question;

import java.util.ArrayList;

public class AnswerActivity extends AppCompatActivity
{
    private int subjectID,questionID;
    private DatabaseSimulator databaseSimulator;
    private int currentUserId;
    private static final int REQUEST_QACREATION = 0;
    private Question question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        subjectID = bundle.getInt("subjectID");
        questionID = bundle.getInt("questionID");
        databaseSimulator = new DatabaseSimulator();
        currentUserId = bundle.getInt("currentUserId");

        question = databaseSimulator.getSubjects().get(subjectID).getQuestions().get(questionID);

        if(question == null) {
            finish();
            return;
        }
        TextView questionTextview = (TextView) findViewById(R.id.question_text_view);
        questionTextview.setText(question.getQuestion());
        TextView  nameTextview  = (TextView) findViewById(R.id.question_user_name);
        nameTextview.setText("- " + question.getUser().getFirstName() +" "+ question.getUser().getLastName() );

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent answerCreationActivity = new Intent(AnswerActivity.this, QACreationActivity.class);
                answerCreationActivity.putExtra("content_type", 1);
                startActivityForResult(answerCreationActivity, REQUEST_QACREATION);

                Snackbar.make(view, "Your answer has been posted???????!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        initializeAnswerList();
    }

    public void initializeAnswerList()
    {
        ArrayList<Answer> answers = new ArrayList<>();
        answers = question.getAnswers();
        ArrayList<String> strings = new ArrayList<>();
        for (Answer answer : answers) {
            strings.add(answer.getAnswer() + " - " + answer.getUser().getFirstName() + " " +answer.getUser().getLastName());
        }

        ListView answerListView = (ListView) findViewById(R.id.answer_list_view);
        answerListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, strings));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == REQUEST_QACREATION) {
            Bundle bundle = data.getExtras();
            String content = bundle.getString("content");
            Answer answer = new Answer(databaseSimulator.getUsers().get(currentUserId), content);
            question.addAnswer(answer);
            initializeAnswerList();
        }
    }
}
